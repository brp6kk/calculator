-- | Defines a method of parsing an Expression from a String.
module Parse where

import Data.Char
import Expression
import Text.ParserCombinators.ReadP

-----------------------------

-- | Main parsing method.
parse :: ReadP Expression
parse = prec1 <* skipSpaces where
    prec6 = unaryr parseFunctions (parseParens prec1) <++
            parseParens prec1 <++ 
            parseAbs prec1 <++ 
            parseDouble <++ 
            parseConstants
    prec5 = unaryl prec6 parseFactorial
    prec4 = chainr1 prec5 parsePower
    prec3 = unaryr parseNegation prec4
    prec2 = chainl1 prec3 $ parseProduct <++ parseQuotient
    prec1 = chainl1 prec2 $ parseSum <++ parseDifference

-----------------------------

-- Precedence 6

parseFunctions :: ReadP (Expression -> Expression)
parseFunctions = parseSine <++ parseCosine <++ parseTangent <++ parseLog <++ parseNatLog

parseSine, parseCosine, parseTangent :: ReadP (Expression -> Expression)
parseSine = "sin" `means` Sine 
parseCosine = "cos" `means` Cosine
parseTangent = "tan" `means` Tangent

parseLog, parseNatLog :: ReadP (Expression -> Expression)
parseLog = "log" `means` Log
parseNatLog = "ln" `means` NatLog

parseParens :: ReadP a -> ReadP a
parseParens = parseBetween (char '(') (char ')')

parseAbs :: ReadP Expression -> ReadP Expression
parseAbs = fmap Absolute . parseBetween bar bar
           where bar = char '|'

parseDouble :: ReadP Expression
parseDouble = skipSpaces *> fmap DoubleValue (read <$> 
    (((.) (++) . (++)) <$> munch1 isDigit <*> string "." <*> munch1 isDigit)
    <++ (munch1 isDigit)
    )

parseConstants :: ReadP Expression
parseConstants = DoubleValue <$> ("pi" `means` pi) <++
                                 ("e" `means` exp 1)

-- Precedence 5

parseFactorial :: ReadP (Expression -> Expression)
parseFactorial = skipSpaces *> pure Factorial <* string "!"

-- Precedence 4

parsePower :: ReadP (Expression -> Expression -> Expression)
parsePower = "^" `means` Power

-- Precedence 3

parseNegation :: ReadP (Expression -> Expression)
parseNegation = "-" `means` Negation

-- Precedence 2

parseProduct, parseQuotient :: ReadP (Expression -> Expression -> Expression)
parseProduct  = "*" `means` Product
parseQuotient = "/" `means` Quotient

-- Precedence 1

parseSum, parseDifference :: ReadP (Expression -> Expression -> Expression)
parseSum        = "+" `means` Sum
parseDifference = "-" `means` Difference

-----------------------------

-- | If String is successfully read, then wrapped f is returned.
--   Use it as an infix operator: string `means` value
means :: String -> f -> ReadP f
means str exp = skipSpaces *> string str *> pure exp

-- | Unary operator that associates to the left.
--   (Applies operator to immediate left value)
unaryl :: ReadP a -> ReadP (a -> a) -> ReadP a
unaryl p op = ((\x f -> f x) <$> p <*> op) <++ p

-- | Unary operator that associates to the right
--   (Applies operator to immediate right value)
unaryr :: ReadP (a -> a) -> ReadP a -> ReadP a
unaryr op p = (op <*> p) <++ p

-- | between function, but with space-skipping capabilities.
parseBetween :: ReadP open -> ReadP close -> ReadP a -> ReadP a
parseBetween open close = between (skipSpaces *> open) (skipSpaces *> close)