-- | Module to test parsing/evaluation.
module Test (runTests) where

import Expression
import Parse
import Text.ParserCombinators.ReadP

-- Pair consisting of input string and expected output.
type IOPair = (String,Either String Double)

runTests :: IO ()
runTests = putStrLn $ check stringAndResult

-- evalAndCompare :: [IOPair] -> [(IOPair,Bool)]
-- getFailures :: [(IOPair,Bool)] -> [IOPair]
-- formatOutput :: [IOPair] -> String
-- addToFailures :: String -> IOPair -> String
-- formatFailures :: IOPair -> String
check :: [IOPair] -> String
check = formatOutput . getFailures . evalAndCompare
    where evalAndCompare x = zip x $ map isExpected x
          getFailures = map fst . filter (not . snd)
          formatOutput list = case list of
                                [] -> "All tests passed."
                                _  -> foldl addToFailures "" list
          addToFailures acc new = acc ++ formatFailure new
          formatFailure (str,exp) = str ++ 
                                    "\nExpected: " ++ show exp ++ 
                                    "\nActual:   " ++ actual str ++ "\n\n"

-- Evaluates string to determine what it actually returns.
actual :: String -> String
actual str = case filter (("" ==) . snd) (readP_to_S parse str) of
                [] -> "No parse."
                [(a,_)] -> show $ eval a
                (_:_:_) -> "Ambiguous parse."

-- Compares evaluated string with expected output.
isExpected :: IOPair -> Bool
isExpected (str,val) = actual str == show val

-----------------------------

stringAndResult :: [IOPair]
stringAndResult = [("4.0",Right 4),
                   ("0.0",Right 0),
                   ("4",Right 4),
                   ("3+2",Right 5),
                   ("3.3+2.2",Right 5.5),
                   ("3.3+2",Right 5.3),
                   ("3.33-2",Right 1.33),
                   ("3.3*2",Right 6.6),
                   ("3.3/2",Right 1.65),
                   (" 3.0 ",Right 3),
                   (" 3 + 2 ",Right 5),
                   ("3 - 1",Right 2),
                   ("3 * 2",Right 6),
                   ("6 / 2",Right 3),
                   ("6 + 2 + 1",Right 9),
                   ("6 + 2 - 1",Right 7),
                   ("6 * 2 * 3",Right 36),
                   ("6 / 2 / 3",Right 1),
                   ("6 + 2 * 3",Right 12),
                   ("6 * 2 + 3",Right 15),
                   ("16 - 2 * 3",Right 10),
                   ("12 + 8 / 2",Right 16),
                   ("6 + 3 * 4 - 77 / 11",Right 11),
                   ("(6 + 3) * 4 - 77 / 11",Right 29),
                   ("55 - ((6 + 3) * 4)",Right 19),
                   ("((((((55 + 2))))))",Right 57),
                   ("4 ^ 2",Right 16),
                   ("4 * 2 ^ 2",Right 16),
                   ("3 ^ 2 ^ 3",Right 6561),
                   ("53 + 2 * 4 - 9 ^ 2 / 3",Right 34),
                   ("(3 ^ 2) ^ 3",Right 729),
                   ("(3.3 ^ 2) ^ 3 + 44.2",Right 1335.6679689999996),
                   ("(3 ^ 2.1) ^ 3 * 4.3",Right 4358.4529321892815),
                   ("(6 + 2) * 3 ^ 3 - 2 ^ 3",Right 208),
                   ("-4",Right (-4)),
                   ("-(4+2)",Right (-6)),
                   ("-3^2",Right (-9)),
                   ("8 + -2",Right 6),
                   ("-8 * -4",Right 32),
                   ("32 - -5",Right 37),
                   ("-5^(4+-2)",Right (-25)),
                   ("pi",Right 3.141592653589793),
                   ("pi ^ 2",Right 9.869604401089358),
                   ("pi / pi",Right 1),
                   ("e",Right 2.718281828459045),
                   ("pi ^ e",Right 22.459157718361045),
                   ("pi + 4 * pi - e ^ 2",Right 8.318907169018317),
                   ("|-4|",Right 4),
                   ("|-4^2*-2+-444|",Right 412),
                   ("|pi * -3|",Right 9.42477796076938),
                   ("4+|3*-2|",Right 10),
                   ("(3 + |-3|) * 2",Right 12),
                   ("4!",Right 24),
                   ("5 + 3! ",Right 11),
                   ("3!^2",Right 36),
                   ("-4!",Right (-24)),
                   ("4!/6",Right 4),
                   ("|5 - 9|!",Right 24),
                   ("sin (pi/2)",Right 1),
                   ("1 + sin (pi/2)",Right 2),
                   ("|sin (3*pi/2) - 23| * (sin (0) + 2)",Right 48),
                   ("8 / sin (3 * pi/2) + -sin(pi/2) - -3^2",Right 0),
                   ("cos (pi) + sin (pi/2)",Right 0),
                   ("cos (pi/2) - (88 + -3^3 - |-32|)",Right (-29)),
                   ("tan (pi/4) + 8 * 3!",Right 49),
                   ("(3 * cos (3)) ^ tan(0)",Right 1),
                   ("cos(4^(1/2)) * tan (-3) + 4!",Right 23.940679707038907),
                   ("ln(e)",Right 1),
                   ("log(10)",Right 1),
                   ("ln (e ^ 2) * -3!",Right (-12)),
                   ("log(10^5) + -ln(e ^ 8) / 2.0",Right 1),
                   ("log(5) / ln(0.5) + 22",Right 20.991599441014287),
                   ("cos (33.2 ^ (1/3) + cos (e)) * ln (log (82)) - log (10)",Right (-1.4335747652859774)),
                   ("tan(pi/2)",Left "Improper domain for Tangent."),
                   ("log (-3)",Left "Logarithm of non-positive number."),
                   ("ln (-42)",Left "Logarithm of non-positive number."),
                   ("(-4)!",Left "Factorial of negative number."),
                   ("(-0.2)!",Left "Factorial of negative number."),
                   ("4.2!",Left "Factorial of fractional number."),
                   ("55 / 0",Left "Division by zero.")
                   ]