-- | Defines an expression and a strategy to evaluate one.
module Expression where

-- | Expression data type.
--   Recursively defined.
data Expression 
    = DoubleValue Double
    | Sine Expression
    | Cosine Expression
    | Tangent Expression
    | Log Expression
    | NatLog Expression
    | Absolute Expression
    | Factorial Expression
    | Power Expression Expression
    | Negation Expression
    | Product Expression Expression
    | Quotient Expression Expression
    | Sum Expression Expression
    | Difference Expression Expression
    deriving (Show)

-----------------------------

-- | Evaluates an expression to a double (Right).
--   A string (Left) is returned if some form of calculation error occurs.
eval :: Expression -> Either String Double
eval (DoubleValue val) = Right val
eval (Sine ex) = unop sin ex
eval (Cosine ex) = unop cos ex
eval (Tangent ex) 
    | undefinedDomain = Left "Improper domain for Tangent."
    | otherwise = unop tan ex
    where undefinedDomain = isInt (ex' / (pi/2)) && ex' /= 0
          ex' = unwrap ex
eval (Log ex)
    | unwrap ex <= 0 = Left "Logarithm of non-positive number."
    | otherwise = unop (logBase 10) ex
eval (NatLog ex)
    | unwrap ex <= 0 = Left "Logarithm of non-positive number."
    | otherwise = unop log ex
eval (Absolute ex) = unop abs ex
eval (Factorial ex) 
    | ex' < 0         = Left "Factorial of negative number."
    | not $ isInt ex' = Left "Factorial of fractional number."
    | otherwise       = unop factorial ex
    where ex' = unwrap ex
eval (Power ex1 ex2) = binop (**) ex1 ex2
eval (Negation ex) = unop negate ex
eval (Product ex1 ex2) = binop (*) ex1 ex2
eval (Quotient ex1 ex2) = case unwrap ex2 of
                            0 -> Left "Division by zero."
                            x -> binop (/) ex1 ex2
eval (Sum ex1 ex2) = binop (+) ex1 ex2
eval (Difference ex1 ex2) = binop (-) ex1 ex2

-----------------------------

-- Helpers

-- | True if a double has no fractional component; false otherwise.
isInt :: Double -> Bool
isInt x = x == fromIntegral (round x)

-- | Factorial - rounds double first to prevent fraction-related errors.
factorial :: Double -> Double
factorial x = fromIntegral $ product [1..(round x)]

-- | Performs unary operation on an expression.
unop :: (Double -> Double) ->  Expression -> Either String Double
unop op ex = op <$> eval ex

-- | Performs binary operation on two expressions.
binop :: (Double -> Double -> Double) -> Expression -> Expression -> Either String Double
binop op ex1 ex2 = op <$> eval ex1 <*> eval ex2

-- | Evaluates an expression and unwraps the underlying double.
--   Note that things will go horribly wrong if the expression evaluates
--   to a Left String, but that doesn't happen with the code I wrote.
unwrap :: Expression -> Double
unwrap = (\(Right x) -> x) . eval