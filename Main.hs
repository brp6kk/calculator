-- | Defines how a user interacts with the calculator.
module Main where

import Data.Char
import Expression
import Parse
import Text.ParserCombinators.ReadP

-- | Runs when Main.exe is started.
main :: IO ()
main = putStrLn startup >> program

-- IO loop
-- Keeps going until "q" is entered.
program :: IO ()
program = do
    str <- getLine
    case map toLower str of
        "q" -> putStrLn "\nGoodbye!"
        "u" -> putStrLn usage >> program
        _   -> putStrLn (calc str ++ "\n") >> program

-- | Performs calculation.
--   Returns result, either an error message or the successful result.
calc :: String -> String
calc str = case filter (("" ==) . snd) (readP_to_S parse str) of
                [] -> "Error - Improper input." -- No parse.
                [(a,_)] -> case eval a of
                                (Left a) -> "Error - " ++ a
                                (Right a) -> show a
                (a:b:cs) -> "Error - Ambiguous input." -- Ambiguous parse.

-- | Messages for user.
usage, startup :: String
startup = "\"u\" for usage\n\"q\" to quit\n"
usage = "\nBoth integer and decimal values are valid input.\n\n" ++
        "x+y    -> Adds x and y\n" ++
        "x-y    -> Subtracts y from x\n" ++
        "x*y    -> Multiplies x and y\n" ++
        "x/y    -> Divides x by y\n" ++
        "x^y    -> x to the power of y\n" ++
        "-x     -> Negates x\n" ++
        "x!     -> Factorial of positive integer x\n" ++
        "|x|    -> Absolute value of x\n" ++
        "sin(x) -> Sine of x (parentheses necessary)\n" ++
        "cos(x) -> Cosine of x (parentheses necessary)\n" ++
        "tan(x) -> Tangent of x (parentheses necessary)\n" ++
        "ln(x)  -> Natural logarithm of x (parentheses necessary)\n" ++
        "log(x) -> Base-10 logarithm of x (parentheses necessary)\n\n" ++
        "The constants e and pi are defined appropriately.\n\n" ++
        "Order of operations is as follows:\n" ++
        "  6. Functions/Parentheses/Absolute Values/Numbers/Constants\n" ++
        "  5. Factorials\n" ++
        "  4. Exponents\n" ++
        "  3. Negations\n" ++
        "  2. Products/Quotients\n" ++
        "  1. Sums/Differences\n" ++
        "Note that \"6\" labels highest precedence while \"1\" labels lowest.\n" ++
        "Items of identical precedence are evaluated from left to right.\n"